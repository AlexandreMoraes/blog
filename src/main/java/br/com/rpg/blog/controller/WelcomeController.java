package br.com.rpg.blog.controller;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WelcomeController {

	@Value("${app.welcome.message}")
	private String message;

	@Value("${app.welcome.title}")
	private String title;

	@RequestMapping("/blog")
	public String welcome(Map<String, Object> model) {
		model.put("title", title);
		model.put("message", message);
		return "welcome";
	}

	@RequestMapping("/blog/5xx")
	public String ServiceUnavailable() {
		throw new RuntimeException();
	}
}