package br.com.rpg.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication
public class BlogRpgApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogRpgApplication.class, args);
	}
}