package br.com.rpg.blog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.rpg.blog.model.PostComment;

@RepositoryRestResource(collectionResourceRel = "postComments", path = "postComments")
public interface PostCommentRepository extends JpaRepository<PostComment, Long> {}