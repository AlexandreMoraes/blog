package br.com.rpg.blog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.rpg.blog.model.PostItem;

@RepositoryRestResource(collectionResourceRel = "postItens", path = "postItens")
public interface PostItemRepository extends JpaRepository<PostItem, Long> {}