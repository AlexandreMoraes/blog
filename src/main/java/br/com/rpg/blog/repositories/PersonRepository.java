package br.com.rpg.blog.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.rpg.blog.model.Person;

@RepositoryRestResource(collectionResourceRel = "person", path = "person")
public interface PersonRepository extends JpaRepository<Person, Long> {

	@Query("select count(p) from Person p")
	public long count();

	public Person findByEmailOrNickNameAndPassword(@Param("email") final String email,
			@Param("nickName") final String nickName, @Param("password") final String password);

	public Page<Person> findByFullName(@Param("fullName") final String fullName, final Pageable pageable);

	public Page<Person> findByFullNameStartsWith(@Param("startsWith") final String startsWith, final Pageable pageable);

	public Page<Person> findByFullNameEndsWith(@Param("endsWith") final String endsWith, final Pageable pageable);

	public Page<Person> findByFullNameContaining(@Param("contains") final String contains, final Pageable pageable);

	public Page<Person> findByNickName(@Param("nickName") final String nickName, final Pageable pageable);

	public Page<Person> findByNickNameStartsWith(@Param("startsWith") final String startsWith, final Pageable pageable);

	public Page<Person> findByNickNameEndsWith(@Param("endsWith") final String endsWith, final Pageable pageable);

	public Page<Person> findByNickNameContaining(@Param("contains") final String contains, final Pageable pageable);

	public Page<Person> findByEmail(@Param("email") final String email, final Pageable pageable);

	public Page<Person> findByEmailStartsWith(@Param("startsWith") final String startsWith, final Pageable pageable);

	public Page<Person> findByEmailEndsWith(@Param("endsWith") final String endsWith, final Pageable pageable);

	public Page<Person> findByEmailContaining(@Param("contains") final String contains, final Pageable pageable);

	public Page<Person> findByFullNameIgnoreCase(@Param("fullName") final String fullName, final Pageable pageable);

	public Page<Person> findByFullNameStartsWithIgnoreCase(@Param("startsWith") final String startsWith,
			final Pageable pageable);
	
	public Page<Person> findByFullNameEndsWithIgnoreCase(@Param("endsWith") final String endsWith,
			final Pageable pageable);

	public Page<Person> findByFullNameContainingIgnoreCase(@Param("contains") final String contains,
			final Pageable pageable);

	public Page<Person> findByNickNameIgnoreCase(@Param("nickName") final String nickName, final Pageable pageable);

	public Page<Person> findByNickNameStartsWithIgnoreCase(@Param("startsWith") final String startsWith,
			final Pageable pageable);

	public Page<Person> findByNickNameEndsWithIgnoreCase(@Param("endsWith") final String endsWith,
			final Pageable pageable);

	public Page<Person> findByNickNameContainingIgnoreCase(@Param("contains") final String contains,
			final Pageable pageable);

	public Page<Person> findByEmailIgnoreCase(@Param("email") final String email, final Pageable pageable);

	public Page<Person> findByEmailEndsWithIgnoreCase(@Param("endsWith") final String endsWith,
			final Pageable pageable);

	public Page<Person> findByEmailContainingIgnoreCase(@Param("contains") final String contains,
			final Pageable pageable);
}