package br.com.rpg.blog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.rpg.blog.model.Tag;

public interface TagRepository extends JpaRepository<Tag, String>{

}
