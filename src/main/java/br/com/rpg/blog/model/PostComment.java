package br.com.rpg.blog.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class PostComment implements Serializable{

	private static final long serialVersionUID = 8052635059020617183L;

	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne(optional=false)
	private Person commentPerson;
	
	@ManyToOne(optional=false)
	private PostItem postItem;
	
	private Date commentDate;
	
	private String content;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Person getCommentPerson() {
		return commentPerson;
	}

	public void setCommentPerson(Person commentPerson) {
		this.commentPerson = commentPerson;
	}
	
	public PostItem getPostItem(){
		return postItem;
	}
	
	public void setPostItem(PostItem postItem){
		this.postItem = postItem;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostComment other = (PostComment) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PostComment [id=" + id + ", commentPerson=" + commentPerson + ", commentDate=" + commentDate
				+ ", content=" + content + "]";
	}
}