package br.com.rpg.blog.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class PostItem implements Serializable{
	
	private static final long serialVersionUID = -7217162925426741989L;

	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne(optional=true)
	private Person postPerson;
	
	private Date publishDate;
	
	private String title;
	
	@ManyToMany
	private List<Tag> tags;
	
	private String content;
	
	@OneToMany
	private List<PostComment> comments;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Person getPostPerson() {
		return postPerson;
	}

	public void setPostPerson(Person postPerson) {
		this.postPerson = postPerson;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<PostComment> getComments() {
		return comments;
	}

	public void setComments(List<PostComment> comments) {
		this.comments = comments;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostItem other = (PostItem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PostItem [id=" + id + ", postPerson=" + postPerson + ", publishDate=" + publishDate + ", title=" + title
				+ ", tags=" + tags + ", content=" + content + ", comments=" + comments + "]";
	}
}