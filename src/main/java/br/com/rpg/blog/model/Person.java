package br.com.rpg.blog.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Person implements Serializable{
	
	private static final long serialVersionUID = 6266012591633898441L;

	@Id
	@GeneratedValue
	private Long id;
	
	private String fullName;
	
	@Column(unique=true)
	private String email;
	
	@Column(unique=true)
	private String nickName;
	
	private String password;
	
	@OneToMany
	private List<PostItem> posts;
	
	@ManyToMany
	private List<Person> frinds;
	
	@ManyToMany(mappedBy="frinds")
	private List<Person> frindsRel;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<PostItem> getPosts() {
		return posts;
	}

	public void setPosts(List<PostItem> posts) {
		this.posts = posts;
	}

	public List<Person> getFrinds() {
		return frinds;
	}

	public void setFrinds(List<Person> frinds) {
		this.frinds = frinds;
	}
	
	public List<Person> getFrindsRel() {
		return frindsRel;
	}
	
	public void setFrindsRel(List<Person> frindsRel) {
		this.frindsRel = frindsRel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", fullName=" + fullName + ", email=" + email + ", nickName=" + nickName
				+ ", password=" + password + ", posts=" + posts + ", frinds=" + frinds + "]";
	}
}